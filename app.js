// HTML Elements 
const getLoanButtonElement = document.getElementById('getLoanButton')
const bankButtonElement = document.getElementById('bankButton')
const workButtonElement = document.getElementById('workButton')
const buyButtonElement = document.getElementById('buyButton')
const repayLoanButtonElement = document.getElementById('repayLoanButton')
const featuresElement = document.getElementById('features')
const laptopsElement = document.getElementById('laptopList')
const imageElement = document.getElementById('image')
const payValElement = document.getElementById('payVal')
const balanceValElement = document.getElementById('balanceVal')
const loanValElement = document.getElementById('loanVal')
const loanTextElement = document.getElementById('loanText')
const priceElement = document.getElementById('price')
const descriptionElement = document.getElementById('description')
const descriptionHeaderElement = document.getElementById('descriptionHeader')

// Variables to store information 
let laptops = []
let balance = 0
let loanCount = 0
let pay = 0
let loan = 0
let price = 0
let fetchURL = "https://noroff-komputer-store-api.herokuapp.com/computers"

// Fetch data from API and add to Menu
fetch(fetchURL)
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))
    
// Function for adding laptops to menu 
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))

    // Update to display information for first laptop in list
    price = laptops[0].price
    priceElement.innerHTML = `${laptops[0].price} NOK`
    descriptionElement.innerHTML = laptops[0].description
    descriptionHeaderElement.innerHTML = laptops[0].title
    updateFeatures(laptops[0].specs)
    updateImage(laptops[0].image, 0)
}

// Function to add single laptop to menu 
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement)
}

// Function to display and add computer features
const updateFeatures = (specs) => {
    // Remove current features 
    while( featuresElement.firstChild ){
        featuresElement.removeChild( featuresElement.firstChild );
      }
    
    // Add new features to list 
    for (const spec of specs){
        const featureElement = document.createElement('li');
        featureElement.innerHTML = spec
        featuresElement.appendChild(featureElement)
    }
}

// Function for updating image display 
const updateImage = (image, index) => {
    imgPath=''
    
    // Change file-type to .png for computer at index 4
    if (index===4){
        imgPath = image.split('.jpg')[0] + '.png'
    }else{
        imgPath=image
    }
    // Set new image-source
    imageElement.setAttribute('src', fetchURL.split("computer")[0] + imgPath)
}

// Function to handle laptop menu change 
const handlelaptopMenuChange = e => {
    // Get selected laptop
    const selectedlaptop = laptops[e.target.selectedIndex]
    
    // Update information to match selected laptop
    price = selectedlaptop.price
    priceElement.innerHTML = `${selectedlaptop.price} NOK`
    descriptionElement.innerHTML = selectedlaptop.description
    descriptionHeaderElement.innerHTML = selectedlaptop.title
    updateFeatures(laptops[e.target.selectedIndex].specs)
    updateImage(laptops[e.target.selectedIndex].image, e.target.selectedIndex)
}

// Function for handeling loan
const handleGetLoan = () => {
    if (loanCount > 0){ // Check if the user has already lent money witout buying a computer
        alert('You cannot get more than one bank loan before buying a computer.')
        return
    }else if( loan !=0){ // Check if the user has any outstanding loan
        alert('You must pay back the current loan before you can get a new one.')
        return
    }else{
        newLoan = parseFloat(prompt('Please enter the amount of money you wish to loan: '))
        if (newLoan > 2*balance){ // Check if loan is more than double of the bank balance
            alert('You cannot get a loan more than double of your bank balance')
            return
        }
        if (!Number.isNaN(newLoan)){ // Continue if loan amout is below limit
            // Update loanCount and balance 
            loanCount+=1
            loan += newLoan
            balance += loan

            // Update HTML-elements
            balanceValElement.innerHTML = `${balance.toFixed(2)} kr.`
            repayLoanButtonElement.className = 'show';
            loanTextElement.innerHTML = 'Loan:'
            loanValElement.innerHTML = `${loan.toFixed(2)} kr.`
        }
    }
}

// Function for handeling work
const handleWork = () => {
    // Update pay by 100
    pay += 100

    // Update HTML elements
    payValElement.innerHTML = `${pay.toFixed(2)} kr.`
}

// Function for handeling banking of money 
const handleBank = () => {
    if (loan > 0){ // Check if user has loan
        
        // Deduct 10 % of pay from outstanding loan
        deducted = 0.1*pay

        if (deducted > loan){ // Check if deducted is greater than loan
            // Add the rest to the balance after the loan is paid
            balance += (deducted-loan)
            loan = 0;
        }else{ // Deduct from loan
            loan -= deducted
        }

        // Add 90 % of pay to bank balance 
        balance += pay*0.9

        // Update HTML element
        loanValElement.innerHTML = `${loan.toFixed(2)} kr.`
    }else{
        // Update balance without deduction money
        balance += pay
    }
    
    // Set pay to zero 
    pay=0

    // Update balance and pay in HTML
    balanceValElement.innerHTML = `${balance.toFixed(2)} kr.`
    payValElement.innerHTML = `${pay.toFixed(2)} kr.`

}

// Function for handeling repaying of loan
const handleRepayLoan = () => {
    // Calculate the rest after paying back loan
    rest = pay - loan

    // Update pay and loan based on the rest
    if (rest > 0){ // Handle when pay > loan 
        pay = rest
        loan = 0
        // Hide repay loan button
        repayLoanButtonElement.className = 'hidden'
    }else if (rest < 0){ // Handle when pay < loan
        loan = -rest
        pay = 0
    }else { // Handle when pay == loan
        pay = 0
        loan = 0
        // Hide repay loan button
        repayLoanButtonElement.className = 'hidden'
    }

    // Update new balance and pay
    loanValElement.innerHTML = `${loan.toFixed(2)} kr.`
    payValElement.innerHTML = `${pay.toFixed(2)} kr.`
}

// Function for handeling buying a computer
const handleBuy = () => {
    // Check if balance is high enough to buy the selected computer
    if (balance < price){
        alert(`Your balance is too low to buy the computer ${descriptionHeaderElement.innerHTML}.`)
        return
    }else {
        // Update new balance
        balance-=price
        balanceValElement.innerHTML = `${balance.toFixed(2)} kr.`
        alert(`Congratz! You are now the new owner of the computer ${descriptionHeaderElement.innerHTML}.`)
        loanCount=0

    }

}

// Event-listeners for buttons
laptopsElement.addEventListener("change", handlelaptopMenuChange)
getLoanButtonElement.addEventListener("click", handleGetLoan)
workButtonElement.addEventListener('click', handleWork)
bankButtonElement.addEventListener('click', handleBank)
repayLoanButtonElement.addEventListener('click', handleRepayLoan)
buyButtonElement.addEventListener('click', handleBuy)